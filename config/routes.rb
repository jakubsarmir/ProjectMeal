Rails.application.routes.draw do
  resources :repositories do
    resources :issues
  end
  get 'pages/main'
  get 'pages/about'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'posts#index'
  get "home/index"

  # sign up page with form:
  get 'users/new' => 'users#new', as: :new_user

  # create (post) action for when sign up form is submitted:
  post 'users' => 'users#create'

  # log in page with form:
  get '/login'     => 'sessions#new'

  # create (post) action for when log in form is submitted:
  post '/login'    => 'sessions#create'

  # delete action to log out:
  delete '/logout' => 'sessions#destroy'

  get 'pages/secret', as: :secret

  get 'repositories/show' => 'repositories#show', as: :show_repo

  #get 'repositories', as: :show_repo

  #get 'repository/new' => 'repository#new', as: :new_repository

  resources :posts
end
