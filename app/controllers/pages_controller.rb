class PagesController < ApplicationController

  # Restrict access so only logged in users can access the secret page:
  before_action :authorize, only: [:secret]

  # ----- end of added lines -----


  def secret
    @repositories = Repository.all
  end

  def main
  end

  def about
  end
end
